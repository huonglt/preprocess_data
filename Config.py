import os

import yaml

yml_file = open(os.path.join(os.path.dirname(__file__), 'config', 'config.yaml'), 'r', encoding="utf-8")
Config = yaml.load(yml_file, Loader=yaml.FullLoader)
yml_file.close()