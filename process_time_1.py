import os

from util import load_config, process_phone, preprocess_name, preprocess_sex
import codecs
import pandas as pd
import numpy as np

Config = load_config()

data_mini_pass = Config["pass"]["data_mini"]
# files_path = codecs.open("tmp/file_plain.txt", "r", encoding="utf-8").read().split("\n")
output_folder = "D:\Telsol\data v4"
input_folder = "D:\Telsol\data v1"
folder = "D:\Telsol"

cols_result = ["new_path", "old_path", "file_name", "sheetname", "col_name", "sample_data",
               "len row ", "num not null data", "col description", "title"]

tag_result_df = pd.read_excel("data/Tagged results.xlsx")
tag_result_df = tag_result_df[tag_result_df.columns[0:11]]
del tag_result_df["new_path"]
tag_result_df = tag_result_df.drop_duplicates(keep='first', inplace=False)
error_paths = []


def detect_header_columns(df):
    remove_index = []
    titles = []
    for i in range(len(df)):
        row = df.loc[i]
        if row[0] is not np.nan:
            try:
                float(row[0])
                remove_index.append(i)
            except:
                tmp_items = []
                for item in row:
                    if type(item) is str:
                        tmp_items.append(item)
                if len(tmp_items) <= 2:
                    titles += tmp_items
                    remove_index.append(i)
                else:
                    break
        else:
            remove_index.append(i)
    df.drop(df.index[remove_index], inplace=True)
    new_header = df.iloc[0]
    df = df[1:]
    df.columns = new_header
    df.reset_index(drop=True, inplace=True)
    return df, titles


def check_cols_is_nan(df):
    for col in df.columns:
        if df[col].notnull().sum() <= 2:
            del df[col]
    return df


def process_file():
    df_results = pd.DataFrame(columns=cols_result)
    files_path = list(set(tag_result_df["old_path"].values))
    for num_path, path in enumerate(files_path):
        # path = "D:\Telsol\data v1\ALL DATA\Data New SG 2020\Data 2020\mk thanhcong\Scenic 2.xls"
        filename = path.split("\\")[-1]
        new_file_name = filename
        if filename.endswith(".csv"):
            new_file_name = filename.replace(".csv", ".xlsx")
        if filename.endswith(".xls"):
            new_file_name = filename.replace(".xls", ".xlsx")
        new_path = os.path.join(output_folder, new_file_name)
        if os.path.exists(new_path):
            for i in range(4000):
                new_name = f"_{i}.".join(new_file_name.split("."))
                new_path = os.path.join(output_folder, new_name)
                if not os.path.exists(new_path):
                    break

        print(path)
        try:
            if path.endswith(".xlsx") or path.endswith(".xls"):
                xls = pd.ExcelFile(path)

                if filename.endswith(".xls"):
                    new_file_name = filename.replace(".xls", ".xlsx")
                else:
                    new_file_name = filename
                new_path = os.path.join(output_folder, new_file_name)
                writer = pd.ExcelWriter(new_path, engine='xlsxwriter')
                for sheet_name in xls.sheet_names:
                    print(sheet_name)
                    df = pd.read_excel(xls, sheet_name=sheet_name, header=None)
                    df = df.dropna(axis=1, how='all')
                    df = check_cols_is_nan(df)
                    if len(df) == 0:
                        continue
                    df, titles = detect_header_columns(df)

                    for i, title in enumerate(titles):
                        df[f"title {i}"] = [title for t in range(len(df))]

                    df.to_excel(writer, engine='xlsxwriter', index=False, encoding="utf-8", sheet_name=sheet_name)
                    for col in df.columns:
                        data = df[col]
                        des_data = data.describe().to_string()
                        num_data = len(df[col].notnull())
                        sample_data = df[col][df[col].notnull().index[0]]
                        row = [new_path.replace(folder, ""), path.replace(input_folder, ""), filename, sheet_name,
                               str(col).lower(), sample_data, len(df[col]), num_data,
                               str(des_data), str(titles)]
                        df_results = df_results.append(pd.Series(row, cols_result), ignore_index=True)
                writer.save()
            elif path.endswith(".csv"):
                df = pd.read_csv(path)
                df = df.dropna(axis=1, how='all')
                df, titles = detect_header_columns(df)

                for i, title in enumerate(titles):
                    df[f"title {i}"] = [title for t in range(len(df))]

                df.to_excel(new_path, index=False, encoding="utf-8")
                for col in df.columns:
                    data = df[col]
                    des_data = data.describe()
                    num_data = len(df[col].notnull())
                    sample_data = df[col][df[col].notnull().index[0]]
                    row = [new_path.replace(folder, ""), path.replace(input_folder, ""), filename, "", str(col).lower(),
                           sample_data, len(df[col]), num_data,
                           str(des_data), str(titles)]
                    df_results = df_results.append(pd.Series(row, cols_result), ignore_index=True)


        except Exception as e:
            print(f"Exception {e}: {path}")
            if path not in error_paths:
                with codecs.open("tmp/error_path.txt", "a", encoding="utf-8") as f:
                    f.write(path)
                    f.write("\n")
                error_paths.append(path)
        if num_path % 5 == 0:
            df_results.to_excel("tmp/sample_result.xlsx", index=False, encoding="utf-8")
            print(num_path)
            print("---------------------------------------------------------------------------------------------------")


def preprocess_file():
    files_path = list(set(tag_result_df["old_path"].values))
    for num_path, path in enumerate(files_path):
        file_path = input_folder + path
        print(file_path)

        try:
            tagged_df = tag_result_df[tag_result_df["old_path"] == path]
            sheet_names = list(set(tagged_df["sheetname"].values))
            _af_df = None
            if "af" in tagged_df["move_column"].values:
                _af_df = tagged_df[tagged_df["move_column"] == "af"]
                _af_df = _af_df[["new col_name", "default value"]]
            if file_path.endswith(".xlsx") or file_path.endswith(".xls"):
                xls = pd.ExcelFile(file_path)
                for sheet_name in sheet_names:
                    print(sheet_name)
                    df = pd.read_excel(xls, sheet_name=sheet_name, header=None)
                    df = check_cols_is_nan(df)
                    if len(df) == 0:
                        continue
                    df, titles = detect_header_columns(df)

                    tmp_df = tagged_df[tagged_df["sheetname"] == sheet_name]
                    remove_df = tmp_df[(tmp_df["move_column"] == "r") | (tmp_df["move_column"] == "c")]
                    for i in remove_df["col_name"].values:
                        for j in df.columns:
                            if type(i) is str and type(j) is str and i.strip().lower() == j.strip().lower():
                                df.drop(j, inplace=True, axis=1)
                            elif i == j:
                                df.drop(j, inplace=True, axis=1)
                    tmp_df = tmp_df[(tmp_df["move_column"] != "r") & (tmp_df["move_column"] != "c")]

                    if 0 in tmp_df["move_column"].values:
                        # đổi tên cột
                        new_col = []
                        _0_df = tmp_df[(tmp_df["move_column"] == 0)]
                        for df_col in df.columns.values:
                            for i_0 in _0_df.index:
                                row = _0_df.loc[i_0]
                                if df_col.lower() == row["col_name"].lower():
                                    new_col.append(row["new col_name"])
                                    break
                        df.columns = new_col

                    if "n" in tmp_df["move_column"].values:
                        _n_df = tmp_df[tmp_df["move_column"] == "n"]
                        first_row = df.columns
                        df.columns = _n_df["new col_name"]
                        df = df.append(first_row, ignore_index=True)
                    if "as" in tmp_df["move_column"].values:
                        # thêm cột mới cho sheet
                        _as_df = tmp_df[(tmp_df["move_column"] == "n")]
                        for item in _as_df:
                            df[item["new col_name"]] = [item["default value"] for _ in range(len(df))]
                        print(0)
                    if _af_df is not None:
                        for i_a in _af_df.index:
                            row = _af_df.loc[i_a]
                            df[row["new col_name"]] = [row["default value"] for _ in range(len(df))]

                    # preprocess cols
                    if "điện thoại" in df.columns:
                        new_phone = []
                        for i in df["điện thoại"].values:
                            new_phone.append(process_phone(i))
                        df["điện thoại"] = new_phone

                    if "họ tên" in df.columns:
                        new_name = []
                        for i in df["họ tên"].values:
                            new_name.append(preprocess_name(i))
                        df["họ tên"] = new_name
                    if "sex" in df.columns:
                        new_sex = []
                        for i in df["sex"].values:
                            new_sex.append(preprocess_sex(i))
                        df["sex"] = new_sex
                    # concat họ và tên
                    if "họ" in df.columns and "tên" in df.columns:
                        df["họ tên"] = df[["họ", "tên"]].agg(' '.join, axis=1)

                    print(0)

            elif file_path.endswith(".csv"):
                df = pd.read_csv(file_path)
                df, titles = detect_header_columns(df)

                remove_df = tagged_df[(tagged_df["move_column"] == "r") | (tagged_df["move_column"] == "c")]
                for i in remove_df["col_name"].values:
                    for j in df.columns:
                        if type(i) is str and type(j) is str and i.strip().lower() == j.strip().lower():
                            df.drop(j, inplace=True, axis=1)
                        elif i == j:
                            df.drop(j, inplace=True, axis=1)
                tagged_df = tagged_df[(tagged_df["move_column"] != "r") & (tagged_df["move_column"] != "c")]

                if 0 in tagged_df["move_column"].values:
                    # đổi tên cột
                    new_col = []
                    _0_df = tagged_df[(tagged_df["move_column"] == 0)]
                    for df_col in df.columns.values:
                        for i_0 in _0_df.index:
                            row = _0_df.loc[i_0]
                            if df_col.lower() == row["col_name"].lower():
                                new_col.append(row["new col_name"])
                                break
                    df.columns = new_col

                if "n" in tagged_df["move_column"].values:
                    _n_df = tagged_df[tagged_df["move_column"] == "n"]
                    first_row = df.columns
                    df.columns = _n_df["new col_name"]
                    df = df.append(first_row, ignore_index=True)
                if "as" in tagged_df["move_column"].values:
                    # thêm cột mới cho sheet
                    _as_df = tagged_df[(tagged_df["move_column"] == "n")]
                    for item in _as_df:
                        df[item["new col_name"]] = [item["default value"] for _ in range(len(df))]
                    print(0)
                if _af_df is not None:
                    for i_a in _af_df.index:
                        row = _af_df.loc[i_a]
                        df[row["new col_name"]] = [row["default value"] for _ in range(len(df))]

                # preprocess cols
                if "điện thoại" in df.columns:
                    new_phone = []
                    for i in df["điện thoại"].values:
                        new_phone.append(process_phone(i))
                    df["điện thoại"] = new_phone

                if "họ tên" in df.columns:
                    new_name = []
                    for i in df["họ tên"].values:
                        new_name.append(preprocess_name(i))
                    df["họ tên"] = new_name
                if "sex" in df.columns:
                    new_sex = []
                    for i in df["sex"].values:
                        new_sex.append(preprocess_sex(i))
                    df["sex"] = new_sex
                # concat họ và tên
                if "họ" in df.columns and "tên" in df.columns:
                    df["họ tên"] = df[["họ", "tên"]].agg(' '.join, axis=1)

        except FileNotFoundError as ex:
            print(f"Exception {ex}: {file_path}")
            with codecs.open("tmp/file_not_found_error_path.txt", "a", encoding="utf-8") as f:
                f.write(file_path)
                f.write("\n")
            error_paths.append(file_path)
        except UnicodeDecodeError as uex:
            print(f"Exception {uex}: {file_path}")
            with codecs.open("tmp/decode_error_path.txt", "a", encoding="utf-8") as f:
                f.write(file_path)
                f.write("\n")
            error_paths.append(file_path)
        except Exception as e:
            print(f"Exception {e}: {file_path}")
            if file_path not in error_paths:
                with codecs.open("tmp/error_path.txt", "a", encoding="utf-8") as f:
                    f.write(file_path)
                    f.write("\n")
                error_paths.append(file_path)

preprocess_file()
