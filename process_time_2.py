import codecs
import os

excel_csv_file_paths = codecs.open("tmp/tmp.txt", encoding="utf-8").read().split("\n")
text_file_paths = [i.strip() for i in codecs.open("tmp/file_not_excel_csv_process.txt", encoding="utf-8").read().split("\n") if not i.endswith(".rar") or not i.endswith(".zip")]

output_path = "D:\Telsol\data_not_process\\text"
# results = []
def process_excel():
    count = 0
    for path in excel_csv_file_paths:
        file_name = path.split("\\")[-1]
        # results.append(file_name)
        new_path = os.path.join(output_path, file_name)
        if os.path.exists(new_path):
            for i in range(4000):
                file_name = f"_{i}.".join(file_name.split("."))
                new_path = os.path.join(output_path, file_name)
                if not os.path.exists(new_path):
                    break
        cmd = f"""copy "{path}" "{new_path}" """
        result = os.system(cmd)
        # prin/t(result)
        if result == 1:
            print(path)
        else:
            count+=1
    print(count)

def process_doc():
    count = 0
    for path in text_file_paths:
        file_name = path.split("\\")[-1]
        # results.append(file_name)
        new_path = os.path.join(output_path, file_name)
        if os.path.exists(new_path):
            for i in range(4000):
                file_name = f"_{i}.".join(file_name.split("."))
                new_path = os.path.join(output_path, file_name)
                if not os.path.exists(new_path):
                    break
        cmd = f"""copy "{path}" "{new_path}" """
        result = os.system(cmd)
        # prin/t(result)
        if result == 1:
            print(path)
        else:
            count += 1
    print(count)

process_doc()