from datetime import datetime
import re
import json

dauso = json.loads(open("config/chuyen_dau_so.json", "r").read())


class PersonProcess:
    def __init__(self, phone_number=None, name=None, lastname=None, first_name=None, gender=None, dob=None,
                 year_of_birth=None,
                 age=None, identity_number=None, identity_place=None, home_phone_number=None, email=None, religion=None,
                 nation=None, hometown=None, faction_date=None, tax_code=None, sport=[], interest=[], woh_name=None,
                 marital_status=None, has_child=None, child=[], temp_address=[], re=[], nationality=None,
                 residential_address=None, degree=[], school_name=[], university_name=[], has_saving=None,
                 saving_amount=[], has_insurance=None, insurance_type=[], has_stock=None, has_remittances=None,
                 has_gold=None, has_invest=None, has_atm=None, credit_limit=[], has_loan=None, loan=[], car=[],
                 intent=[], organization_loyalty=[], club=[], vt=[], org=[], position=[], job=[], income=None):
        self.phone_number = phone_number
        self.name = name
        self.lastname = lastname
        self.first_name = first_name
        self.gender = gender
        self.dob = dob
        self.year_of_birth = year_of_birth
        self.age = age
        self.identity_number = identity_number
        self.identity_place = identity_place
        self.home_phone_number = home_phone_number
        self.email = email
        self.religion = religion
        self.nation = nation
        self.hometown = hometown
        self.faction_date = faction_date
        self.tax_code = tax_code
        self.sport = sport
        self.interest = interest
        self.woh_name = woh_name
        self.marital_status = marital_status
        self.has_child = has_child
        self.child = child  # object
        self.nationality = nationality
        self.residential_address = residential_address
        self.temp_address = temp_address  # object
        self.degree = degree
        self.school_name = school_name
        self.university_name = university_name
        self.has_saving = has_saving
        self.saving_amount = saving_amount
        self.has_insurance = has_insurance
        self.insurance_type = insurance_type
        self.has_stock = has_stock
        self.has_remittances = has_remittances
        self.has_gold = has_gold
        self.has_invest = has_invest
        self.has_atm = has_atm
        self.credit_limit = credit_limit
        self.has_loan = has_loan
        self.loan = loan
        self.car = car
        self.intent = intent
        self.re = re
        self.vt = vt
        self.organization_loyalty = organization_loyalty
        self.club = club
        self.org = org
        self.position = position
        self.job = job
        self.income = income

    def preprocess_gender(self, gender):
        if gender.lower().strip() == "mr" or gender.lower().strip() == "mr." or gender.lower().strip() == "mr." or gender.lower().strip() == "ông" or gender.lower().strip() == "nam" or gender.lower().strip() == "anh" or gender.lower().strip() == "male":
            self.gender = "Nam"
        elif gender.lower().strip() == "mrs" or gender.lower().strip() == "ms" or gender.lower().strip() == "ms." or gender.lower().strip() == "mrs." or gender.lower().strip() == "bà" or gender.lower().strip() == "nữ" or gender.lower().strip() == "nu" or gender.lower().strip() == "chị" or gender.lower().strip() == "female":
            self.gender = "Nữ"
        else:
            print(f"Giới tính: {gender}")
            self.gender = "KXĐ"

    def preprocess_name(self, name):
        if name.lower().startswith("ông/bà"):
            name = name.lower().replace("ông/bà", "").strip()
        if name.lower().startswith("mr/mrs"):
            name = name.lower().replace("mr/mrs", "").strip()
        if name.lower().startswith("anh/chị"):
            name = name.lower().replace("anh/chị", "").strip()
        if name.lower().startswith("a/c"):
            name = name.lower().replace("a/c", "").strip()
        if name.lower().startswith("a "):
            name = name.lower().replace("a ", "").strip()
            self.gender = "Nam"
        if name.lower().startswith("c "):
            name = name.lower().replace("c ", "").strip()
            self.gender = "Nữ"
        if name.lower().startswith("a."):
            name = name.lower().replace("a. ", "").strip()
            self.gender = "Nam"
        if name.lower().startswith("c."):
            name = name.lower().replace("c. ", "").strip()
            self.gender = "Nữ"
        if name.lower().startswith("mr."):
            name = name.lower().replace("mr. ", "").strip()
            self.gender = "Nam"
        if name.lower().startswith("mrs."):
            name = name.lower().replace("mrs. ", "").strip()
            self.gender = "Nữ"
        if name.lower().startswith("chị"):
            name = name.lower().replace("chị ", "").strip()
            self.gender = "Nữ"
        if name.lower().startswith("ông"):
            name = name.lower().replace("ông ", "").strip()
            self.gender = "Nam"
        if name.lower().startswith("bà"):
            name = name.lower().replace("bà ", "").strip()
            self.gender = "Nữ"
        if "," in name:
            name = name.split(",")[0].strip()
        if "(" in name:
            name = name.split("(")[-1].strip()
            name = name.split(")")[0].strip()
        name = name.title()
        if "-" in name:
            name = name.split("-")[0].strip()
        if "/" in name:
            name = name.split("/")[0].strip()
        self.name = name

    def process_phone(self, phone_number):
        """
        Process phone number to number
        :param phone_number:
        :return:
        """

        phone_number = str(phone_number).strip()
        if ":" in phone_number:
            tmp = phone_number.strip(":")
            if not tmp[0][0].isalpha():
                phone_number = tmp[0]
        if "," in phone_number:
            phone_number = phone_number.split(",")[0]

        if phone_number.endswith(")"):
            tmp = phone_number.split("(")
            if len(tmp[0]) >= 8:
                phone_number = tmp[0]
            else:
                phone_number = tmp[-1]
            phone_number = phone_number.split(")")[0]
        if phone_number.startswith("("):
            phone_number.replace("(", "").replace(")", "")
        if ";" in phone_number:
            phone_number = phone_number.split(";")[0].strip()
        if " " in phone_number:
            tmp = phone_number.split(" ")
            if len(tmp[0]) >= 8:
                phone_number = tmp[0].strip()
            elif len(tmp[-1]) >= 8:
                phone_number = tmp[-1].strip()
            else:
                phone_number = phone_number.replace(" ", "")
        if "." in phone_number:
            phone_number = phone_number.replace(".", "")
        if "~" in phone_number:
            phone_number = phone_number.split("~")[0]
        if "/" in phone_number:
            phone_number = phone_number.split("/")[0]
        if "-" in phone_number:
            temp = phone_number.split("-")
            if len(temp[-1]) > 4:
                phone_number = temp[0].strip()
            else:
                phone_number = "".join(temp)
        if "'" in phone_number:
            phone_number = phone_number.replace("'", "")
        if phone_number.startswith("+84"):
            phone_number.replace("+84", "0")
        if not phone_number.startswith("0") and not phone_number.startswith("+"):
            phone_number = "0" + phone_number
        for k, v in dauso.items():
            if phone_number.startswith(k):
                phone_number.replace(k, v)

        self.phone_number = phone_number

    def process_marital_status(self, marital_status):
        """
        U: Undefined: Không xác định
        S: single: Độc thân
        M: married: Đã kết hôn
        D: Divorced: Đã ly hôn
        W: Widowed: Góa vợ
        :param marital_status:
        :return:
        """
        if marital_status.upper().strip() == "U":
            self.marital_status = "KXĐ"
        elif marital_status.upper().strip() == "S":
            self.marital_status = "Độc Thân"
        elif marital_status.upper().strip() == "M":
            self.marital_status = "Đã kết hôn"
        elif marital_status.upper().strip() == "D":
            self.marital_status = "Đã ly hôn"
        elif marital_status.upper().strip() == "W":
            self.marital_status = "Góa vợ"
        elif marital_status.lower().strip() == "độc thân":
            self.marital_status = "Độc thân"
        elif marital_status.upper().strip() == "đã kết hôn":
            self.marital_status = "Đã kết hôn"
        elif marital_status.upper().strip() == "đã ly hôn":
            self.marital_status = "Đã ly hôn"
        elif marital_status.upper().strip() == "góa vợ":
            self.marital_status = "Góa vợ"
        else:
            self.marital_status = "KXĐ"

    def process_dob(self, dob):
        format_dates = []
        if len(dob) == 4:
            try:
                dob = int(dob)
                self.year_of_birth = dob
            except:
                pass
        else:
            if " " in dob:
                dob = dob.split(" ")[0]
            if "(" in dob:
                dob = dob.split("(")[0]
            if "-" in dob:
                temp = dob.split("-")
                if len(temp[0]) > 4:
                    dob = temp[0].strip()
                    if "/" in dob:
                        temp = dob.split("/")
                        if len(temp[0]) <= 2:
                            if len(temp) == 2:
                                try:
                                    self.dob = datetime.strptime(dob, '%d/%m')
                                except:
                                    try:
                                        self.dob = datetime.strptime(dob, '%m/%d')
                                    except:
                                        print(f"{dob} not recognize")
                            else:
                                try:
                                    self.dob = datetime.strptime(dob, '%d/%m/%Y')
                                    self.year_of_birth = self.dob.year
                                except:
                                    self.dob = datetime.strptime(dob, '%m/%d/%Y')
                                    self.year_of_birth = self.dob.year
                        else:
                            try:
                                self.dob = datetime.strptime(dob, '%Y/%m/%d')
                                self.year_of_birth = self.dob.year
                            except:
                                self.dob = datetime.strptime(dob, '%Y/%d/%m')
                                self.year_of_birth = self.dob.year
                else:
                    if len(temp[0]) <= 2:
                        try:
                            self.dob = datetime.strptime(dob, '%d-%m-%Y')
                            self.year_of_birth = self.dob.year
                        except:
                            try:
                                self.dob = datetime.strptime(dob, '%m-%d-%Y')
                                self.year_of_birth = self.dob.year
                            except:
                                try:
                                    self.dob = datetime.strptime(dob, '%m-%d-%y')
                                    self.year_of_birth = self.dob.year
                                except:
                                    try:
                                        self.dob = datetime.strptime(dob, '%d-%m-%y')
                                        self.year_of_birth = self.dob.year
                                    except:
                                        try:
                                            self.dob = datetime.strptime(dob, '%d-%B-%y')
                                            self.year_of_birth = self.dob.year
                                        except:
                                            try:
                                                self.dob = datetime.strptime(dob, '%d-%B-%Y')
                                                self.year_of_birth = self.dob.year
                                            except:
                                                try:
                                                    self.dob = datetime.strptime(dob, '%d-%b-%Y')
                                                    self.year_of_birth = self.dob.year
                                                except:
                                                    try:
                                                        self.dob = datetime.strptime(dob, '%d-%b-%y')
                                                        self.year_of_birth = self.dob.year
                                                    except:
                                                        try:
                                                            self.dob = datetime.strptime(dob, '%B-%d-%Y')
                                                            self.year_of_birth = self.dob.year
                                                        except:
                                                            try:
                                                                self.dob = datetime.strptime(dob, '%b-%d-%Y')
                                                                self.year_of_birth = self.dob.year
                                                            except:
                                                                print(f"{dob} not recognize")

                    else:
                        try:
                            self.dob = datetime.strptime(dob, '%Y-%m-%d')
                            self.year_of_birth = self.dob.year
                        except:
                            self.dob = datetime.strptime(dob, '%Y-%d-%m')
                            self.year_of_birth = self.dob.year
            elif "." in dob:
                temp = dob.split(".")
                if len(temp[0]) <= 2:
                    try:
                        self.dob = datetime.strptime(dob, '%d.%m.%Y')
                        self.year_of_birth = self.dob.year
                    except:
                        self.dob = datetime.strptime(dob, '%m.%d.%Y')
                        self.year_of_birth = self.dob.year
                else:
                    try:
                        self.dob = datetime.strptime(dob, '%Y.%m.%d')
                        self.year_of_birth = self.dob.year
                    except:
                        self.dob = datetime.strptime(dob, '%Y.%d.%m')
                        self.year_of_birth = self.dob.year
            elif "/" in dob:
                temp = dob.split("/")
                if len(temp) <= 2:
                    try:
                        self.dob = datetime.strptime(dob, '%d/%m')
                    except:
                        try:
                            self.dob = datetime.strptime(dob, '%m/%d')
                        except:
                            print(f"{dob} not recognize")
                elif len(temp[0]) <= 2:
                    try:
                        self.dob = datetime.strptime(dob, '%d/%m/%Y')
                        self.year_of_birth = self.dob.year
                    except:
                        try:
                            self.dob = datetime.strptime(dob, '%m/%d/%Y')
                            self.year_of_birth = self.dob.year
                        except:
                            try:
                                self.dob = datetime.strptime(dob, '%m/%d/%y')
                                self.year_of_birth = self.dob.year
                            except:
                                self.dob = datetime.strptime(dob, '%d/%m/%y')
                                self.year_of_birth = self.dob.year
                else:
                    try:
                        self.dob = datetime.strptime(dob, '%Y/%m/%d')
                        self.year_of_birth = self.dob.year
                    except:
                        self.dob = datetime.strptime(dob, '%Y/%d/%m')
                        self.year_of_birth = self.dob.year
        pass

    def clean_text(self, rgx_list, text):
        new_text = text
        for rgx_match in rgx_list:
            new_text = re.sub(rgx_match, '', new_text)
        return new_text

    def process_age(self, age):
        if age != None:
            self.age = int(age)

    def process_firstname(self, first_name):
        self.first_name = first_name

    def process_lastname(self, last_name):
        self.lastname = last_name

    def process_identity_number(self, identity_number):
        self.identity_number = identity_number

    def process_has_remittances(self, has_remittances):
        self.has_remittances = has_remittances

    def process_has_gold(self, has_gold):
        self.has_gold = has_gold

    def process_identity_place(self, identity_place):
        self.identity_place = identity_place

    def process_homephone(self, home_phone):
        self.home_phone_number = home_phone

    def process_email(self, email):
        self.email = email

    def process_religion(self, religion):
        self.religion = religion

    def process_nation(self, nation):
        self.nation = nation

    def process_hometown(self, hometown):
        self.hometown = hometown

    def process_faction_date(self, faction_date):
        self.faction_date = faction_date

    def process_tax_code(self, tax_code):
        self.tax_code = tax_code

    def process_sport(self, sport):
        self.sport.append(sport)

    def process_interest(self, interest):
        self.interest.append(interest)

    def process_woh_name(self, woh_name):
        self.woh_name = woh_name

    def process_child(self, json_data):
        child_birth = None
        child_school = None
        child_class = None
        if child_birth in json_data.keys():
            child_birth = json_data['child_birth']
        if child_school in json_data.keys():
            child_school = json_data['child_school']
        if child_class in json_data.keys():
            child_class = json_data['child_class']
        child = Child(child_birth, child_school, child_class)
        self.child.append(child)

    def process_haschild(self, haschild):
        self.haschild = haschild

    def process_nationality(self, nationality):
        self.nationality = nationality

    def process_residential_address(self, residential_address):
        self.residential_address = residential_address

    def process_temp_address(self, json_data):
        temp_address = None
        temp_area = None
        temp_city = None

        temp_district = None
        temp_ward = None
        temp_street = None
        temp_zone = None
        temp_floor = None
        temp_house_number = None
        if 'temp_address' in json_data.keys():
            temp_address = json_data["temp_address"]
        if "temp_area" in json_data.keys():
            temp_area = json_data["temp_area"]
        if "temp_city" in json_data.keys():
            temp_city = json_data["temp_city"]

        if "temp_district" in json_data.keys():
            temp_district = json_data["temp_district"]
        if "temp_ward" in json_data.keys():
            temp_ward = json_data["temp_ward"]
        if "temp_street" in json_data.keys():
            temp_street = json_data["temp_street"]
        if "temp_zone" in json_data.keys():
            temp_zone = json_data['temp_zone']
        if "temp_floor" in json_data.keys():
            temp_floor = json_data['temp_floor']
        if "temp_house_number" in json_data.keys():
            temp_house_number = json_data['temp_house_number']
        tmp_add = TempAddress(temp_address, temp_area, temp_city,
                              temp_district, temp_ward, temp_street, temp_zone, temp_floor,
                              temp_house_number)
        self.temp_address.append(tmp_add)

    def process_degree(self, degree):
        if degree not in self.degree:
            self.degree.append(degree)

    def process_school_name(self, school_name):
        if school_name not in self.school_name:
            self.school_name.append(school_name)

    def process_university_name(self, university_name):
        if university_name not in self.university_name:
            self.university_name.append(university_name)

    def process_has_saving(self, has_saving):
        self.has_saving = has_saving

    def process_has_invest(self, has_invest):
        self.has_invest = has_invest

    def process_has_atm(self, has_atm):
        self.has_atm = has_atm

    def process_credit_limit(self, credit_limit):
        self.credit_limit.append(credit_limit)

    def process_has_loan(self, has_loan):
        self.has_loan = has_loan

    def process_loan(self, json_data):
        loan_amount = None
        loan_type = None
        if "loan_amount" in json_data.keys():
            loan_amount = json_data['loan_amount']
        if "loan_type" in json_data.keys():
            loan_type = json_data["loan_type"]
        self.loan.append(Loan(loan_amount, loan_type))

    def process_car(self, json_data):
        has_car = None
        num_car_or_bike = None
        car_spec = None

        if "has_car" in json_data.keys():
            has_car = json_data["has_car"]
        if "num_car_or_bike" in json_data.keys():
            num_car_or_bike = json_data["num_car_or_bike"]
        if car_spec in json_data['car_spec']:
            car_spec = json_data["car_spec"]
        self.car.append(Car(has_car, num_car_or_bike, car_spec))

    def process_re(self, json_data):
        re = None
        re_price = None
        re_unit_price = None
        re_rank = None
        re_area = None
        re_type = None
        if "re" in json_data.keys():
            re = json_data["re"]
        if "re" in json_data.keys():
            re = json_data["re"]
        if "re_price" in json_data.keys():
            re_price = json_data["re_price"]
        if "re_unit_price" in json_data.keys():
            re_unit_price = json_data["re_unit_price"]
        if "re_rank" in json_data.keys():
            re_rank = json_data["re_rank"]
        if "re_area" in json_data.keys():
            re_area = json_data["re_area"]
        if "re_type" in json_data.keys():
            re_type = json_data["re_type"]
        self.re.append(Apartment(re, re_price, re_unit_price, re_rank, re_area, re_type))

    def process_vt(self, json_data):
        vt_sim_type = None
        vt_amount = None
        vt_device = None
        vt_device_os = None
        vt_datapackage = None
        if "vt_sim_type" in json_data.keys():
            vt_sim_type = json_data["vt_sim_type"]
        if "vt_amount" in json_data.keys():
            vt_amount = json_data["vt_amount"]
        if "vt_device" in json_data.keys():
            vt_device = json_data["vt_device"]
        if "vt_device_os" in json_data.keys():
            vt_device_os = json_data["vt_device_os"]
        if "vt_datapackage" in json_data.keys():
            vt_datapackage = json_data["vt_datapackage"]
        self.vt.append(Telecom(vt_sim_type, vt_amount, vt_device, vt_device_os, vt_datapackage))

    def process_org(self, json_data):
        org_name = None
        org_branch_name = None
        org_address = None
        org_district = None
        org_city = None
        org_type = None
        org_industry = None
        org_website = None
        org_email = None
        org_phone = None
        org_fax = None
        org_tax_code = None
        org_size = None
        org_corporate_capital = None
        if "org_name" in json_data.keys():
            org_name = json_data["org_name"]
        if "org_branch_name" in json_data.keys():
            org_branch_name = json_data["org_branch_name"]
        if "org_address" in json_data.keys():
            org_address = json_data["org_address"]
        if "org_district" in json_data.keys():
            org_district = json_data["org_district"]
        if "org_city" in json_data.keys():
            org_city = json_data["org_city"]
        if "org_type" in json_data.keys():
            org_type = json_data["org_type"]
        if "org_industry" in json_data.keys():
            org_industry = json_data["org_industry"]
        if "org_website" in json_data.keys():
            org_website = json_data["org_website"]
        if "org_email" in json_data.keys():
            org_email = json_data["org_email"]
        if "org_phone" in json_data.keys():
            org_phone = json_data["org_phone"]
        if "org_fax" in json_data.keys():
            org_fax = json_data["org_fax"]
        if "org_tax_code" in json_data.keys():
            org_tax_code = json_data["org_tax_code"]
        if "org_size" in json_data.keys():
            org_size = json_data["org_size"]
        if "org_corporate_capital" in json_data.keys():
            org_corporate_capital = json_data["org_corporate_capital"]
        self.org.append(
            Organization(org_name, org_branch_name, org_address, org_district, org_city, org_type, org_industry,
                         org_website, org_email, org_phone, org_fax, org_tax_code, org_size, org_corporate_capital))

    def process_saving_amount(self, saving_amount):
        if saving_amount not in self.saving_amount:
            self.saving_amount.append(saving_amount)

    def process_has_insurance(self, has_insurance):
        self.has_insurance = has_insurance

    def process_insurance_type(self, insurance_type):
        if insurance_type not in self.insurance_type:
            self.insurance_type.append(insurance_type)

    def process_has_stock(self, has_stock):
        self.has_stock = has_stock

    def process_intent(self, intent):
        if intent not in self.intent:
            self.intent.append(intent)

    def process_organization_loyalty(self, organization_loyalty):
        if organization_loyalty not in self.organization_loyalty:
            self.organization_loyalty.append(organization_loyalty)

    def process_club(self, club):
        if club not in self.club:
            self.club.append(club)

    def process_position(self, position):
        if position not in self.position:
            self.position.append(position)

    def process_job(self, job):
        if job not in self.job:
            self.job.append(job)

    def process_income(self, income):
        self.income = income

    def to_json(self):
        return {"phone_number": self.phone_number,
                "name": self.name,
                "lastname": self.lastname,
                "first_name": self.first_name,
                "gender": self.gender,
                "dob": self.dob,
                "year_of_birth": self.year_of_birth,
                "age": self.age,
                "identity_number": self.identity_number,
                "identity_place": self.identity_place,
                "home_phone_number": self.home_phone_number,
                "email": self.email,
                "religion": self.religion,
                "nation": self.nation,
                "hometown": self.hometown,
                "faction_date": self.faction_date,
                "tax_code": self.tax_code,
                "sport": self.sport,
                "interest": self.interest,
                "woh_name": self.woh_name,
                "marital_status": self.marital_status,
                "has_child": self.has_child,
                "child": [i.to_json() for i in self.child],  # object
                "nationality": self.nationality,
                "residential_address": self.residential_address,
                "temp_address": [i.to_json() for i in self.temp_address],  # object
                "degree": self.degree,
                "school_name": self.school_name,
                "university_name": self.university_name,
                "has_saving": self.has_saving,
                "saving_amount": self.saving_amount,
                "has_insurance": self.has_insurance,
                "insurance_type": self.insurance_type,
                "has_stock": self.has_stock,
                "has_remittances": self.has_remittances,
                "has_gold": self.has_gold,
                "has_invest": self.has_invest,
                "has_atm": self.has_atm,
                "credit_limit": self.credit_limit,
                "has_loan": self.has_loan,
                "loan": [i.to_json() for i in self.loan],
                "car": [i.to_json() for i in self.car],
                "intent": self.intent,
                "re": [r.to_json() for r in self.re],
                "vt": [i.to_json() for i in self.vt],
                "organization_loyalty": self.organization_loyalty,
                "club": self.club,
                "org": [i.to_json() for i in self.org],
                "position": self.position,
                "job": self.job,
                "income": self.income}


class Child:
    def __init__(self, child_birth=None, child_school=None, child_class=None):
        self.child_birth = child_birth
        self.child_school = child_school
        self.child_class = child_class

    def to_json(self):
        return {"child_birth": self.child_birth,
                "child_school": self.child_school,
                "child_class": self.child_class}


class TempAddress:
    def __init__(self, temp_address=None, temp_area=None, temp_city=None, temp_district=None, temp_ward=None,
                 temp_street=None, temp_zone=None, temp_floor=None, temp_house_number=None):
        self.temp_address = temp_address
        self.temp_area = temp_area
        self.temp_city = temp_city
        self.temp_district = temp_district
        self.temp_ward = temp_ward
        self.temp_street = temp_street
        self.temp_zone = temp_zone
        self.temp_floor = temp_floor
        self.temp_house_number = temp_house_number

    def to_json(self):
        return {"temp_address": self.temp_address,
                'temp_area': self.temp_area,
                "temp_city": self.temp_city,
                "temp_district": self.temp_district,
                "temp_ward": self.temp_ward,
                "temp_street": self.temp_street,
                "temp_zone": self.temp_zone,
                "temp_floor": self.temp_floor,
                "temp_house_number": self.temp_house_number}


class Loan:
    def __init__(self, loan_amount=None, loan_type=None):
        self.loan_amount = loan_amount
        self.loan_type = loan_type

    def to_json(self):
        return {"loan_amount": self.loan_amount,
                "loan_type": self.loan_type}


class Car:
    def __init__(self, has_car=None, num_car_or_bike=None, car_spec=None):
        self.has_car = has_car
        self.num_car_or_bike = num_car_or_bike
        self.car_spec = car_spec

    def to_json(self):
        return {
            "num_car_or_bike": self.num_car_or_bike,
            "car_spec": self.car_spec}


class Apartment:
    def __init__(self, re=None, re_price=None, re_unit_price=None, re_rank=None, re_area=None, re_type=None):
        self.re_price = re_price
        self.re_unit_price = re_unit_price
        self.re_rank = re_rank
        self.re_area = re_area
        self.re_type = re_type
        self.re = re

    def to_json(self):
        return {"re_price": self.re_price,
                "re_unit_price": self.re_unit_price,
                "re_rank": self.re_rank,
                "re_area": self.re_area,
                "re_type": self.re_type,
                "re": self.re}


class Telecom:
    def __init__(self, vt_sim_type=None, vt_amount=None, vt_device=None, vt_device_os=None,
                 vt_datapackage=None):
        self.vt_sim_type = vt_sim_type
        self.vt_amount = vt_amount
        self.vt_device = vt_device
        self.vt_device_os = vt_device_os
        self.vt_datapackage = vt_datapackage

    def to_json(self):
        return {"vt_sim_type": self.vt_sim_type,
                "vt_amount": self.vt_amount,
                "vt_device": self.vt_device,
                "vt_device_os": self.vt_device_os,
                "vt_datapackage": self.vt_datapackage}


class Organization:
    def __init__(self, org_name=None, org_branch_name=None, org_address=None,
                 org_district=None, org_city=None, org_type=None, org_industry=None, org_website=None,
                 org_email=None,
                 org_phone=None, org_fax=None, org_tax_code=None, org_size=None, org_corporate_capital=None):
        self.org_name = org_name
        self.org_branch_name = org_branch_name
        self.org_address = org_address
        self.org_district = org_district
        self.org_city = org_city
        self.org_type = org_type
        self.org_industry = org_industry
        self.org_website = org_website
        self.org_email = org_email
        self.org_phone = org_phone
        self.org_fax = org_fax
        self.org_tax_code = org_tax_code
        self.org_size = org_size
        self.org_corporate_capital = org_corporate_capital

    def to_json(self):
        return {"org_name": self.org_name,
                "org_branch_name": self.org_branch_name,
                "org_address": self.org_address,
                "org_district": self.org_district,
                "org_city": self.org_city,
                "org_type": self.org_type,
                "org_industry": self.org_industry,
                "org_website": self.org_website,
                "org_email": self.org_email,
                "org_phone": self.org_phone,
                "org_fax": self.org_fax,
                "org_tax_code": self.org_tax_code,
                "org_size": self.org_size,
                "org_corporate_capital": self.org_corporate_capital}
#
# dob = ["1970-03-06 00:00:00",
#        "01/01/1956",
#        "1986",
#        "18.8.1982",
#        "16-10-1976",
#        "14/03/1976",
#        "29/01/85",
#        "29/02",
#        "20/12/1960-11/12/1967",
#        "02/05/1974 (Hòa) \n 03/10/1974 (Thư)",
#        "26-Jan-66 00:00:00"]
# person = PersonProcess()
#
# for d in dob:
#     person.process_dob(d)
#     print(person.dob)
#     print(person.year_of_birth)
#     print()
