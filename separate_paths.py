import codecs
import os

output_folder = "D:\Telsol\data v3"
input_folder = "D:\Telsol\data v1"


def list_all_paths(path):
    file_names = os.listdir(path)
    folders = []
    files = []
    others = []
    files_paths = [os.path.join(path, file) for file in file_names]
    for file_path in files_paths:
        if os.path.isdir(file_path):
            print("folder: ", file_path)
            folders.append(file_path)
        elif os.path.isfile(file_path):
            print("file: ", file_path)
            files.append(file_path)
        else:
            print("other: ", file_path)
            others.append(file_path)
    return files, folders, others


run_paths = [input_folder]
f_file = codecs.open("tmp/file_excel_csv.txt", "a", "utf-8")
f_others = codecs.open("tmp/file_not_excel_csv.txt", "a", "utf-8")
for i in run_paths:
    files, folders, others = list_all_paths(i)
    run_paths += folders
    for file in files:
        if file.lower().endswith(".xlsx") or file.lower().endswith("xls") or file.lower().endswith(".csv"):
            f_file.write(file)
            f_file.write("\n")
        else:
            f_others.write(file)
            f_others.write("\n")
    for file in others:
        f_others.write(file)
        f_others.write("\n")

