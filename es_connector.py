from elasticsearch import Elasticsearch, helpers
from util import load_config
Config = load_config()
es_config = Config["publish_es"]


class ElasticSearchConnection:
    def __init__(self, host=es_config["host"], port=es_config["port"], es_index=es_config["index"],
                 doc_type=es_config["doc_type"], timeout=100):
        if es_config["push"]:
            self.elasticsearch = Elasticsearch(hosts=f"{host}:{port}", timeout=timeout)
        else:
            self.elasticsearch = None
        self.es_index = es_index
        self.doc_type = doc_type

    def insert(self, person):
        if self.elasticsearch is None:
            return
        self.elasticsearch.index(index=self.es_index, doc_type=self.doc_type, id=person.phone_number, body=person.to_json())

    def insert_phone(self, phone_number):
        if self.elasticsearch is None:
            return
        self.elasticsearch.index(index=self.es_index, doc_type=self.doc_type, id=phone_number, body={"phone_number": phone_number})

    def upsert(self, person):
        if self.elasticsearch is None:
            return

        self.elasticsearch.update_by_query(body=person.to_json(), doc_type="person", index=self.doc_type, id=person.phone_number)

    def bulk_insert(self, people):
        if self.elasticsearch is None:
            return
        data = []
        for person in people:
            data.append({
                "_index": self.es_index,
                "_type": self.doc_type,
                "_id": person["phone_number"],
                "_source": person,
                "doc_as_upsert": True
            })
            if len(data) == 10000:
                helpers.bulk(self.elasticsearch, data)
                data.clear()
        if len(data) > 0:
            helpers.bulk(self.elasticsearch, data)

    def fetch(self, id):
        if self.elasticsearch is None:
            return
        page = self.elasticsearch.search(
            index=self.es_index,
            body={

                    "query": {
                        "match": {
                            "phone_number": {
                                "query": id
                            }
                        }
                    }


            }
        )
        # sid = page['_scroll_id']
        hits = page['hits']['hits']
        if len(hits)==1:
            return hits[0]['_source']
        else:
            return None
        # scroll_size = page['hits']['total']
        # while scroll_size > 0:
        #     page = self.elasticsearch.scroll(scroll_id=sid, scroll='10m')
        #     sid = page['_scroll_id']
        #     scroll_size = len(page['hits']['hits'])
        #     hits = page['hits']['hits']
        #     for post in hits:
        #         return post['_source']



    @staticmethod
    def fetch_all(es_host, es_port, es_index, es_timeout=100):
        es = Elasticsearch(f"{es_host}:{es_port}", timeout=es_timeout)
        page = es.search(
            index=es_index,
            scroll='10m',
            size=10000,

            body={
                "_source": True
            }
        )
        sid = page['_scroll_id']
        hits = page['hits']['hits']
        for post in hits:
            yield post['_source']
        scroll_size = page['hits']['total']
        while scroll_size > 0:
            page = es.scroll(scroll_id=sid, scroll='10m')
            sid = page['_scroll_id']
            scroll_size = len(page['hits']['hits'])
            hits = page['hits']['hits']
            for post in hits:
                yield post['_source']
