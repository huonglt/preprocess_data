import os
import yaml
import numpy as np
import re
import pandas as pd


def load_config():
    """
    load config from config.yaml file
    :return:
    """
    yml_file = open(os.path.join(os.path.dirname(__file__), 'config', 'config.yaml'), 'r', encoding="utf-8")
    Config = yaml.load(yml_file, Loader=yaml.FullLoader)
    yml_file.close()
    return Config


# def convert_xls_to_xlsx(input_file_path, output_folder_path):
#     """
#     Convert .xls file to .xlsx file
#     :param input_file_path: .xls file
#     :param output_folder_path: output folder
#     :return:
#     """
#     fname = input_file_path.split("\\")[-1]
#     if not input_file_path.endswith(".xls"):
#         raise Exception("Wrong input file extension")
#     excel = win32.gencache.EnsureDispatch('Excel.Application')
#     wb = excel.Workbooks.Open(input_file_path)
#
#     wb.SaveAs(os.path.join(output_folder_path, fname + "x"), FileFormat=51)  # FileFormat = 51 is for .xlsx extension
#     wb.Close()  # FileFormat = 56 is for .xls extension
#     excel.Application.Quit()


def detect_header_columns(df):
    remove_index = []
    titles = []
    for i in df.index:
        row = df.loc[i]
        if row[0] is not np.nan:
            try:
                float(row[0])
                remove_index.append(i)
            except:
                tmp_items = []
                for item in row:
                    if type(item) is str:
                        tmp_items.append(item)
                if len(tmp_items) <= 2:
                    titles += tmp_items
                    remove_index.append(i)
                else:
                    break
        else:
            remove_index.append(i)
    df.drop(df.index[remove_index], inplace=True)
    new_header = df.iloc[0]
    df = df.iloc[1:]
    df.columns = new_header
    df.reset_index(drop=True, inplace=True)
    return df, titles


def clean_text(rgx_list, text):
    new_text = text
    for rgx_match in rgx_list:
        new_text = re.sub(rgx_match, '', new_text)
    return new_text


def process_phone(phone_number):
    """
    Process phone number to number
    :param phone_number:
    :return:
    """
    # if phone_number.startswith("+84"):
    #     phone_number.replace("+84", "09")
    phone_number = phone_number.strip()
    if phone_number.endswith(")"):
        phone_number = phone_number.split("(")[0]
    if phone_number.startswith("("):
        phone_number.replace("(", "").replace(")", "")
    if " " in phone_number:
        phone_number = phone_number.replace(" ", "")
    if "~" in phone_number:
        phone_number = phone_number.split("~")[0]
    if "/" in phone_number:
        phone_number = phone_number.split("/")[0]
    if "-" in phone_number:
        temp = phone_number.split("-")
        if len(temp[-1]) > 4:
            phone_number = temp[0].strip()
        else:
            phone_number = "".join(temp)
    if "'" in phone_number:
        phone_number = phone_number.replace("'", "")
    if phone_number.startswith("+84"):
        phone_number.replace("+84", "0")
    if not phone_number.startswith("0") and not phone_number.startswith("+"):
        phone_number = "0" + phone_number
    return phone_number


def preprocess_sex(sex):
    if sex.lower() == "mr" or sex.lower() == "mr." or sex.lower() == "ông" or sex.lower() == "nam" or sex.lower() == "anh":
        return "Nam"
    elif sex.lower() == "mrs" or sex.lower() == "mrs." or sex.lower() == "bà" or sex.lower() == "nữ" or sex.lower() == "chị":
        return "Nữ"
    else:
        print(f"Giới tính: {sex}")
        return "KXĐ"


def preprocess_name(name):
    if name.lower().startswith("ông/bà"):
        name = name.lower().replace("ông/bà", "").strip()
    if name.lower().startswith("mr/mrs"):
        name = name.lower().replace("mr/mrs", "").strip()
    if name.lower().startswith("anh/chị"):
        name = name.lower().replace("anh/chị", "").strip()
    if name.lower().startswith("a/c"):
        name = name.lower().replace("a/c", "").strip()
    if name.lower().startswith("a "):
        name = name.lower().replace("a ", "").strip()
    if name.lower().startswith("c "):
        name = name.lower().replace("c ", "").strip()
    if name.lower().startswith("a."):
        name = name.lower().replace("a. ", "").strip()
    if name.lower().startswith("c."):
        name = name.lower().replace("c. ", "").strip()
    if name.lower().startswith("mr."):
        name = name.lower().replace("mr. ", "").strip()
    if name.lower().startswith("mrs."):
        name = name.lower().replace("mrs. ", "").strip()
    if name.lower().startswith("chị"):
        name = name.lower().replace("chị ", "").strip()
    if name.lower().startswith("ông"):
        name = name.lower().replace("ông ", "").strip()
    if name.lower().startswith("bà"):
        name = name.lower().replace("bà ", "").strip()
    name = name.title()
    return name


# names = ["HOÀNG THỊ SỬU",
# "Giang Xuân Hương",
# "Nguyễn Thị Trâm",
# "thu trinh",
# "Lê Văn Lâm",
# "a phạm hoàng tuấn - VIP",
# "Anh Minh",
# "a công",
# "anh Đức",
# "A Thắng",
# "A phong",
# "A. Hoàng Minh Thái"
# ]
# for name in names:
#     print(preprocess_name(name))
# print(preprocess_sex("MR"))
# nums = [
# "08 8213 - 020 (Ext 503)",
# "08 8225 - 757",
# "08 9256 - 795",
# "08 4041 - 836~7",
# "08 4135 - 226~7",
# "08 8213 - 211",
# "08 8223 - 944",
# "08 8229 - 463",
# "0650 - 576 - 939 / 576925",
# "08 9256 - 885",
# "08 8211 - 111",
# "08 8122 - 790",
# "08 8276 - 035",
# "08 2605 - 674 / 2605668",
# "08 7223 - 523",
# "08 8754 - 446",
# "08 7950 - 591~4",
# "08 7507 - 190~2",
# "08 8966 - 936 / 8960966",
# "072 870 - 363",
# "08 8105 - 276",
# "08 7560 - 691 / 8869959 / 886",
# "08 8227 - 842",
# "08 8977 - 791",
# "08 8755 - 195~6",
# "08 896 - 2720~2 / 8969890",
# "08 8975 - 066 (Ext 101)",
# "08 9206 - 489~90 / 9206859",
# "0908630057(Anh)",
# "907166304",
# "903801478",
# "903915901",
# "913976570",
# "913673307",
# "903889485",
# "913925184",
# "0912900678(Li)",
# "908822229",
#
# ]
# for n in nums:
#     print(process_phone(n))

def readfile(path, sheet_name=None):
    df = None
    if path.endswith(".xls"):
        xls = pd.ExcelFile(path)
        if sheet_name is not None:
            df = pd.read_excel(xls, sheet_name=sheet_name, header=None)
        else:
            df = pd.read_excel(xls, header=None)
        df = df.dropna(axis=1, how='all')
        df = df.dropna(axis=0, how='all')
        df, titles = detect_header_columns(df)
        if len(titles)>0:
            for i, title in enumerate(titles):
                df[f"title {i}"] = [title for t in range(len(df))]

    elif path.endswith(".xlsx"):
        if sheet_name is not None:
            df = pd.read_excel(path, sheet_name=sheet_name, header=None, engine='openpyxl')
        else:
            df = pd.read_excel(path, header=None, engine='openpyxl')
        df = df.dropna(axis=1, how='all')
        df = df.dropna(axis=0, how='all')
        df, titles = detect_header_columns(df)
        if len(titles)>0:
            for i, title in enumerate(titles):
                df[f"title {i}"] = [title for t in range(len(df))]
    elif path.endswith(".csv"):
        df = pd.read_csv(path)
        df = df.dropna(axis=1, how='all')
        df = df.dropna(axis=0, how='all')
        df, titles = detect_header_columns(df)

        for i, title in enumerate(titles):
            df[f"title {i}"] = [title for t in range(len(df))]
    else:
        print(path)

    return df
