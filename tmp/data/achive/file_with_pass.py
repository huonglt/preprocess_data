import os

from util import load_config
import codecs
import pandas as pd
import numpy as np

Config = load_config()

data_mini_pass = Config["pass"]["data_mini"]
files_path = codecs.open("tmp/file_plain.txt", "r", encoding="utf-8").read().split("\n")
output_folder = "D:\Telsol\data v2"
input_folder = "D:\Telsol\data v1"
folder = "D:\Telsol"


# def detect_header_columns(df):
#     for i in range(len(df)):
#         row = df.loc[i]
#         if row[0] is not np.nan:
#             if row.notnan()


def detect_header(df):
    titles = []
    try:
        index_null_all_items = df[(df.isnull().sum(axis=1) == len(df.columns))].index
        df.drop(df.index[index_null_all_items], inplace=True)
        df.reset_index(drop=True, inplace=True)
        index_first_row = df[(df.isnull().sum(axis=1) >= len(df.columns) / 2)].index
        removed_index = []

        if index_first_row[0] > 0:

            for i in range(index_first_row[0]):
                row = df.loc[i]
                for item in row:
                    if item is not np.nan:
                        titles.append(item)
                removed_index.append(i)
        df.drop(df.index[list(set(removed_index))], inplace=True)
        df.reset_index(drop=True, inplace=True)
        removed_index = []
        for i in range(len(df)):
            if df.iloc[i][0] is np.nan:
                removed_index.append(i)
            else:
                try:
                    float(df.iloc[i][0])
                    removed_index.append(i)
                except:
                    break
        df.drop(df.index[list(set(removed_index))], inplace=True)
        df.reset_index(drop=True, inplace=True)
        new_header = df.iloc[0]
        df = df[1:]
        df.columns = new_header
        for i, title in enumerate(titles):
            df[f"title {i}"] = [title for t in range(len(df))]
    except Exception as e:
        print(f"Exception {e}")
        with codecs.open("tmp/error_path.txt", "a", encoding="utf-8") as f:
            f.write(path)
            f.write("\n")

    return df, titles


cols_result = ["new_path", "old_path", "file_name", "sheetname", "col_name", "sample_data",
               "len row ", "num not null data", "col description", "title"]

if __name__ == '__main__':
    df_results = pd.DataFrame(columns=cols_result)

    for path in files_path:
        path = "D:\Telsol\data v1\ALL DATA\Data New SG 2020\Data 2020\mk thanhcong\Scenic 2.xls"
        filename = path.split("\\")[-1]
        print(filename)
        print(path)
        try:
            if path.endswith(".xlsx") or path.endswith(".xls"):
                xls = pd.ExcelFile(path)

                if filename.endswith(".xls"):
                    new_file_name = filename.replace(".xls", ".xlsx")
                else:
                    new_file_name = filename
                new_path = os.path.join(output_folder, new_file_name)
                writer = pd.ExcelWriter(new_path, engine='xlsxwriter')
                for sheet_name in xls.sheet_names:
                    print(sheet_name)
                    df = pd.read_excel(xls, sheet_name=sheet_name, header=None)
                    df.dropna(axis=1, how='all', inplace=True)
                    df.dropna(axis=0, how='all', inplace=True)
                    if len(df) == 0:
                        continue
                    df, titles = detect_header(df)

                    for i, title in enumerate(titles):
                        df[f"title {i}"] = [title for t in range(len(df))]

                    df.to_excel(writer, engine='xlsxwriter', index=False, encoding="utf-8", sheet_name=sheet_name)
                    for col in df.columns:
                        data = df[col]
                        des_data = data.describe().to_string()
                        num_data = len(df[col].notnull())
                        sample_data = df[col][df[col].notnull().index[0]]
                        row = [new_path.replace(folder, ""), path.replace(input_folder, ""), filename, sheet_name,
                               str(col).lower(), sample_data, len(df[col]), num_data,
                               str(des_data), str(titles)]
                        df_results = df_results.append(pd.Series(row, cols_result), ignore_index=True)
                writer.save()
            elif path.endswith(".csv"):
                df = pd.read_csv(path)
                df, titles = detect_header(df)

                for i, title in enumerate(titles):
                    df[f"title {i}"] = [title for t in range(len(df))]

                new_file_name = filename.replace(".csv", ".xlsx")
                new_path = os.path.join(output_folder, new_file_name)
                df.to_excel(new_path, index=False, encoding="utf-8")
                for col in df.columns:
                    data = df[col]
                    des_data = data.describe()
                    num_data = len(df[col].notnull())
                    sample_data = df[col][df[col].notnull().index[0]]
                    row = [new_path.replace(folder, ""), path.replace(input_folder, ""), filename, "", str(col).lower(),
                           sample_data, len(df[col]), num_data,
                           str(des_data), str(titles)]
                    df_results = df_results.append(pd.Series(row, cols_result), ignore_index=True)


        except Exception as e:
            print(f"Exception {e}: {path}")
            with codecs.open("tmp/error_path.txt", "a", encoding="utf-8") as f:
                f.write(path)
                f.write("\n")

    df_results.to_excel("tmp/sample_result.xlsx", index=False, encoding="utf-8")
