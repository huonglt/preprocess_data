import os

import pandas as pd
import xlwings as xw

from process_time_1 import check_cols_is_nan, detect_header_columns
from util import load_config
import codecs
import string
Config = load_config()

data_mini_pass = Config["pass"]["data_mini"]

output_folder = "D:\Telsol\data v5"
input_folder = "D:\Telsol\data v1"

file_with_pass = ""

#
# def _process(filepath, filename):
#     try:
#         new_path = os.path.join(output_folder, filename)
#         writer = pd.ExcelWriter(new_path, engine='xlsxwriter')
#         count = 0
#         try:
#             app = xw.apps.active
#             wb = xw.Book(filepath, password=data_mini_pass[0])
#             wb.save(filepath)
#             try:
#                 for idi in range(wb.sheets.count):
#                     try:
#                         sheet = wb.sheets[idi]
#                         sheet_name = sheet.name.split("!")[0]
#                         sheet_name = sheet_name.translate(str.maketrans('', '', string.punctuation))
#                     except:
#                         sheet_name = "Sheet1"
#                     print(sheet_name)
#                     df = sheet.used_range.options(pd.DataFrame, index=False, header=False).value
#                     if len(df) > 0:
#                         df.to_excel(writer, sheet_name=sheet_name, encoding="utf-8",  index=False, header=False)
#                         count+= len(df)
#
#                     # wb.close()
#                     app.quit()
#             except :
#                 print(0)
#         except:
#             try:
#                 wb = xw.Book(filepath, password=data_mini_pass[1])
#                 app = xw.apps.active
#                 wb.save(filepath)
#                 for i in range(wb.sheets.count):
#                     try:
#                         sheet = wb.sheets[i]
#                     except:
#                         break
#                     try:
#                         sheet_name = wb.sheets[i].name.split("!")[0]
#                         sheet_name = sheet_name.translate(str.maketrans('', '', string.punctuation))
#                     except:
#                         sheet_name = "Sheet1"
#                     print(sheet_name)
#                     df = sheet.used_range.options(pd.DataFrame, index=False, header=False).value
#                     if len(df) > 0:
#                         df.to_excel(writer, sheet_name=sheet_name, encoding="utf-8", index=False, header=False)
#                         count += len(df)
#                     # wb.close()
#                     app.quit()
#             except:
#                 wb = xw.Book(filepath)
#                 app = xw.apps.active
#                 wb.save(filepath)
#                 for i in range(wb.sheets.count):
#                     try:
#                         sheet = wb.sheets[i]
#                     except:
#                         break
#                     try:
#                         sheet_name = wb.sheets[i].name.split("!")[0]
#                         sheet_name = sheet_name.translate(str.maketrans('', '', string.punctuation))
#                     except:
#                         sheet_name = "Sheet1"
#                     print(sheet_name)
#                     df = sheet.used_range.options(pd.DataFrame, index=False, header=False).value
#                     if len(df) > 0:
#                         df.to_excel(writer, sheet_name=sheet_name, encoding="utf-8", index=False, header=False)
#                         count += len(df)
#                     # wb.close()
#                     app.quit()
#             else:
#                 print(filepath)
#         try:
#             writer.save()
#             writer.close()
#         except Exception as e:
#             print(e)
#     except:
#         return False
#     return os.path.join(output_folder, filename), count


def read_protected_file(filepath):
    results = {}
    for passw in data_mini_pass:
        try:
            # new_path = filepath.replace(input_folder, output_folder)
            wb = xw.Book(filepath, password=passw)
            app = xw.apps.active
            wb.save(filepath)
            for i in range(wb.sheets.count):
                try:
                    sheet = wb.sheets[i]
                except:
                    break
                try:
                    sheet_name = wb.sheets[i].name.split("!")[0]
                    sheet_name = sheet_name.translate(str.maketrans('', '', string.punctuation))
                except:
                    sheet_name = "Sheet1"
                print(sheet_name)
                df = sheet.used_range.options(pd.DataFrame, index=False, header=False).value
                results[sheet_name] = df
                app.quit()
            break
        except:
            print(path)
    return results


cols_result = ["new_path", "old_path", "file_name", "sheetname", "col_name", "sample_data",
               "len row ", "num not null data", "col description", "title"]
if __name__ == '__main__':
    paths = codecs.open("tmp/achive/preprocess_times_2.txt", "r", encoding="utf-8").read().split("\n")
    paths = [path.strip() for path in paths]
    df_results = pd.DataFrame(columns=cols_result)

    pahts = list(set(paths))
    print(len(paths))
    for count_path, path in enumerate(paths):
        print(path)
        try:
            filename = path.split("\\")[-1]
            if filename.endswith(".xls"):
                new_file_name = filename.replace(".xls", ".xlsx")
            else:
                new_file_name = filename

            results = read_protected_file(path)
            new_path = os.path.join(output_folder, new_file_name)
            if os.path.exists(new_path):
                for i in range(4000):
                    new_name = f"_{i}.".join(new_file_name.split("."))
                    new_path = os.path.join(output_folder, new_name)
                    if not os.path.exists(new_path):
                        break
            writer = pd.ExcelWriter(new_path, engine='xlsxwriter')
            for sheet_name, df_x in results.items():
                df = check_cols_is_nan(df_x)
                if len(df) == 0:
                    continue
                df, titles = detect_header_columns(df)

                for i, title in enumerate(titles):
                    df[f"title {i}"] = [title for t in range(len(df))]

                df.to_excel(writer, engine='xlsxwriter', index=False, encoding="utf-8", sheet_name=sheet_name)
                for col in df.columns:
                    data = df[col]
                    des_data = data.describe().to_string()
                    num_data = len(df[col].notnull())
                    sample_data = df[col].iloc[df[col].notnull().index[0]]
                    row = [new_path.replace(input_folder, ""), path.replace(input_folder, ""), filename, sheet_name,
                           str(col).lower(), sample_data, len(df[col]), num_data,
                           str(des_data), str(titles)]
                    df_results = df_results.append(pd.Series(row, cols_result), ignore_index=True)
            if count_path%5 ==0 and count_path!=0:
                df_results.to_excel("tmp/sample_result_pass.xlsx", index=False, encoding="utf-8")
                print(count_path)
                print("---------------------------------------------------------------------------------------------------")
        except Exception as e:
            print(e)