import codecs
import pandas as pd

file_paths = codecs.open("tmp/file_excel_csv.txt", encoding="utf-8").read().split("\n")
f_plain = codecs.open("tmp/file_plain.txt", "a", encoding="utf-8")
for path in file_paths:
    try:
        if path.endswith(".xls"):
            with codecs.open(path, encoding = "ISO-8859-1") as f:
                f.read()
        else:
            pd.read_excel(path)
        f_plain.write(path)
        f_plain.write("\n")
    except Exception as e:
            print(e)
            print(path)
