import re

from Object import PersonProcess
from util import readfile, load_config
import pandas as pd
from es_connector import ElasticSearchConnection

Config = load_config()
input_folder = Config["root"]["input_folder"]
f_sdt = open("tmp/sdt.xlsx", "w")
es = ElasticSearchConnection()
vietnamese_reg = "^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$"


def load_result_df():
    tag_result_df = pd.read_excel("data/Tagged results.xlsx", engine='openpyxl')
    tag_result_df = tag_result_df[tag_result_df.columns[0:11]]
    del tag_result_df["new_path"]
    tag_result_df = tag_result_df.drop_duplicates(keep='first')
    return tag_result_df


def run_preprocess_cols(df, tagged_result_df, sheetname=None):
    if sheetname is not None:
        tagged_df = tagged_result_df[tagged_result_df["sheetname"] == sheetname]
    else:
        tagged_df = tagged_result_df
    _af_df = None
    if "af" in tagged_result_df["move_column"].values:
        _af_df = tagged_result_df[tagged_result_df["move_column"] == "af"]
        _af_df = _af_df[["new col_name", "default value"]]
    remove_df = tagged_df[(tagged_df["move_column"] == "r") | (tagged_df["move_column"] == "c")]
    for i in remove_df["col_name"].values:
        for j in df.columns:
            if type(i) is str and type(j) is str and i.strip().lower() == j.strip().lower():
                df = df.drop(j, axis=1)
            elif i == j:
                df = df.drop(j, axis=1)
    tmp_df = tagged_df[(tagged_df["move_column"] != "r") & (tagged_df["move_column"] != "c")]

    if "n" in tmp_df["move_column"].values:
        _n_df = tmp_df[tmp_df["move_column"] == "n"]
        first_row = df.columns
        df.columns = _n_df["new col_name"]
        df = df.append(first_row, ignore_index=True)

    if 0 in tmp_df["move_column"].values:
        # đổi tên cột
        new_col = []
        _0_df = tmp_df[(tmp_df["move_column"] == 0)]
        for df_col in df.columns:
            for i_0 in _0_df.index:
                row = _0_df.loc[i_0]
                if type(df_col) is str and df_col.lower() == row["col_name"].lower():
                    df.rename(columns={df_col: row["new col_name"]}, inplace=True)
                elif df_col == row["col_name"]:
                    df.rename(columns={df_col: row["new col_name"]}, inplace=True)
        df.dropna(axis=1, inplace=True, how="all")
        df.dropna(axis=0, inplace=True, how="all")

    if "as" in tmp_df["move_column"].values:
        # thêm cột mới cho sheet
        _as_df = tmp_df[(tmp_df["move_column"] == "n")]
        for item in _as_df:
            df[item["new col_name"]] = [item["default value"] for _ in range(len(df))]
    if _af_df is not None:
        for i_a in _af_df.index:
            row = _af_df.loc[i_a]
            df[row["new col_name"]] = [row["default value"] for _ in range(len(df))]
    return df


def run_process_person(row):
    row = row.dropna()
    row = row.astype(str)
    person = PersonProcess(car=[], org=[], vt=[], loan=[], re=[], child=[], temp_address=[], sport=[], interest=[],
                           degree=[], school_name=[], university_name=[], saving_amount=[], insurance_type=[],
                           intent=[], organization_loyalty=[], club=[], position=[], job=[], credit_limit = [])
    if "điện thoại" in row.index:
        # if type(row["điện thoại"]) is not str:
        #     return None
        person.process_phone(row["điện thoại"])
        f_sdt.write(row["điện thoại"])
        f_sdt.write("\n")
    if "họ tên" in row.index:
        person.preprocess_name(row["họ tên"])
    if "họ" in row.index:
        person.process_lastname(row["họ"])
    if "tên" in row.index:
        person.process_firstname(row["tên"])
    if "sex" in row.index:
        person.preprocess_gender(row["sex"])
    if "dob" in row.index:
        person.process_dob(row["dob"])
    if "tuổi" in row.index:
        person.process_age(row["tuổi"])
    if "cmnd" in row.index:
        person.process_identity_number(row["cmnd"])
    if "cmnd-nc" in row.index:
        person.process_identity_place(row["cmnd-nc"])
    if "home điện thoại" in row.index:
        person.process_homephone(row["home điện thoại"])
    if "email" in row.index:
        person.process_email(row["email"])
    if "tôn giáo" in row.index:
        person.process_religion(row["tôn giáo"])
    if "dân tộc" in row.index:
        person.process_nation(row["dân tộc"])
    if "quê" in row.index:
        person.process_hometown(row["quê"])
    if "đảng-date" in row.index:
        person.process_faction_date(row["đảng-date"])
    if "mst" in row.index:
        person.process_tax_code(row["mst"])
    if "sport" in row.index:
        person.process_sport(row["sport"])
    if "interest" in row.index:
        person.process_interest(row["interest"])
    if "vợ/chồng" in row.index:
        person.process_woh_name(row["vợ/chồng"])
    if "marital status" in row.index:
        person.process_marital_status(row["marital status"])
    if "haschild" in row.index:
        person.process_haschild(row["haschild"])
    if "quốc tịch" in row.index:
        person.process_nationality(row["quốc tịch"])
    if "địa chỉ-hk" in row.index:
        person.process_residential_address(row["địa chỉ-hk"])
    if "bằng cấp" in row.index:
        person.process_degree(row["bằng cấp"])
    if "trường" in row.index:
        person.process_school_name(row["trường"])
    if "trường-đh" in row.index:
        person.process_university_name(row["trường-đh"])
    if "tiết kiệm" in row.index:
        person.process_has_saving(row["tiết kiệm"])
    if "tiết kiệm-amount" in row.index:
        person.process_saving_amount(row["tiết kiệm-amount"])
    if "bảo hiểm" in row.index:
        person.process_has_insurance(row["bảo hiểm"])
    if "loại bảo hiểm" in row.index:
        person.process_insurance_type(row["loại bảo hiểm"])
    if "chứng khoán" in row.index:
        person.process_has_stock(row["chứng khoán"])
    if "kiều hối" in row.index:
        person.process_has_remittances(row["kiều hối"])
    if "vàng" in row.index:
        person.process_has_gold(row["vàng"])
    if "đầu tư" in row.index:
        person.process_has_invest(row["đầu tư"])
    if "thẻ" in row.index:
        person.process_has_atm(row["thẻ"])
    if "credit-limit" in row.index:
        person.process_credit_limit(row["credit-limit"])
    if "vay" in row.index:
        person.process_has_loan(row["vay"])
    if "intent" in row.index:
        person.process_intent(row["intent"])
    if "loyalty" in row.index:
        person.process_organization_loyalty(row["loyalty"])
    if "clb" in row.index:
        person.process_club(row["clb"])
    if "chức vụ" in row.index:
        person.process_position(row["chức vụ"])
    if "nghề nghiệp" in row.index:
        person.process_job(row["nghề nghiệp"])
    if "thu nhập" in row.index:
        person.process_income(row["thu nhập"])
    if "child-birth" in row.index or "child-school" in row.index or "child-class" in row.index:
        t = {}
        if "child-birth" in row.index:
            t["child_birth"] = row["child-birth"]
        if "child-school" in row.index:
            t["child_school"] = row["child-school"]
        if "child-class" in row.index:
            t["child_class"] = row["child-class"]
        person.process_child(t)
    if "địa chỉ" in row.index or "vùng" in row.index or "tỉnh" in row.index or "quận" in row.index or "phường" in row.index or "đường" in row.index or "khu" in row.index or "tầng" in row.index or "số nhà" in row.index:
        t = {}
        if "địa chỉ" in row.index:
            t["temp_address"] = row["địa chỉ"]
        if "vùng" in row.index:
            t["temp_area"] = row["vùng"]
        if "tỉnh" in row.index:
            t["temp_city"] = row["tỉnh"]
        if "quận" in row.index:
            t["temp_district"] = row["quận"]
        if "phường" in row.index:
            t["temp_ward"] = row["phường"]
        if "đường" in row.index:
            t["temp_street"] = row["đường"]
        if "khu" in row.index:
            t["temp_zone"] = row["khu"]
        if "tầng" in row.index:
            t["temp_floor"] = row["tầng"]
        if "số nhà" in row.index:
            t["temp_house_number"] = row["số nhà"]
        person.process_temp_address(t)
    if "vay-amount" in row.index or "vay-type" in row.index:
        t = {}
        if "vay-amount" in row.index:
            t["loan_amount"] = row["vay-amount"]
        if "vay-type" in row.index:
            t["loan_type"] = row["vay-type"]
        person.process_loan(t)

    if "car" in row.index or "car/bike" in row.index or "car-spec" in row.index:
        t = {}
        if "car" in row.index:
            t["has_car"] = row["car"]
        if "car/bike" in row.index:
            t["num_car_or_bike"] = row["car/bike"]
        if "car-spec" in row.index:
            t["car_spec"] = row["car-spec"]
        person.process_car(t)
    if "re" in row.index or "re-price" in row.index or "re-đg" in row.index or "re-rank" in row.index or "re-dt " in row.index or "re-type" in row.index:
        t = {}
        if "re" in row.index:
            t["re"] = row["re"]
        if "re-price" in row.index:
            t["re_price"] = row["re-price"]
        if "re-đg" in row.index:
            t["re_unit_price"] = row["re-đg"]
        if "re-rank" in row.index:
            t["re_rank"] = row["re-rank"]
        if "re-dt" in row.index:
            t["re_area"] = row["re-dt"]
        if "re-type" in row.index:
            t["re_type"] = row["re-type"]
        person.process_re(t)
    if "vt-package" in row.index or "vt-credit" in row.index or "vt-device" in row.index or "vt-deviceos" in row.index or "vt-datapackage" in row.index:
        t = {}
        if "vt-package" in row.index:
            t["vt_sim_type"] = row["vt-package"]
        if "vt-credit" in row.index:
            t["vt_amount"] = row["vt-credit"]
        if "vt-device" in row.index:
            t["vt_device"] = row["vt-device"]
        if "vt-deviceos" in row.index:
            t["vt_device_os"] = row["vt-deviceos"]
        if "vt-datapackage" in row.index:
            t["vt_datapackage"] = row["vt-datapackage"]
        person.process_vt(t)
    if "cq-tên" in row.index or "cq-tên-cn" in row.index or "cq-đc" in row.index or "cq-quận" in row.index or "cq-tỉnh" in row.index or "cq-loại" in row.index or "cq-ngành" in row.index or "cq-website" in row.index or "cq-email" in row.index or "cq-đt" in row.index or "cq-fax" in row.index or "cq-mst" in row.index or "cq-quy mô" in row.index or "cq-vốn" in row.index:
        t = {}
        if "cq-tên" in row.index:
            t["org_name"] = row["cq-tên"]
        if "cq-tên-cn" in row.index:
            t["org_branch_name"] = row["cq-tên-cn"]
        if "cq-đc" in row.index:
            t["org_address"] = row["cq-đc"]
        if "cq-quận" in row.index:
            t["org_district"] = row["cq-quận"]
        if "cq-tỉnh" in row.index:
            t["org_city"] = row["cq-tỉnh"]
        if "cq-loại" in row.index:
            t["org_type"] = row["cq-loại"]
        if "cq-ngành" in row.index:
            t["org_industry"] = row["cq-ngành"]
        if "cq-website" in row.index:
            t["org_website"] = row["cq-website"]
        if "cq-email" in row.index:
            t["org_email"] = row["cq-email"]
        if "cq-đt" in row.index:
            t["org_phone"] = row["cq-đt"]
        if "cq-fax" in row.index:
            t["org_fax"] = row["cq-fax"]
        if "cq-mst" in row.index:
            t["org_tax_code"] = row["cq-mst"]
        if "cq-quy mô" in row.index:
            t["org_size"] = row["cq-quy mô"]
        if "cq-vốn" in row.index:
            t["org_corporate_capital"] = row["cq-vốn"]
        person.process_org(t)
    return person


def priority_field(person, data):
    if "name" in data.keys():
        if person.name is not None:
            if data["name"] is not None:
                if len(data["name"]) > len(person.name) and re.match(vietnamese_reg, data["name"]):
                    person.name = data["name"]
        else:
            person.name = data["name"]
    if "gender" in data.keys():
        if person.gender is not None:
            if data['gender'] is not None:
                if person.gender == "KXĐ" and data["gender"] != "KXĐ":
                    person.gender = data["gender"]
        else:
            person.gender = data["gender"]
    if "age" in data.keys():
        if data["age"] is not None:
            if person.age is not None:
                if person.age < data['age']:
                    person.age = data["age"]

    pri_ms = {"KXĐ": 0, "Độc Thân": 1, "Đã kết hôn": 2, "Đã ly hôn": 3, "Góa vợ": 4}
    if "marital_status" in data.keys():
        if data["marital_status"] is not None:
            if person.marital_status is not None:
                if pri_ms[data["marital_status"]] > pri_ms[person.marital_status]:
                    person.marital_status = data["marital_status"]
    return person


def insert_to_es():
    tag_result_df = load_result_df()
    files_path = list(set(tag_result_df["old_path"].values))
    # files_path = ['\\ALL DATA\\DATA KHACH HANG\\KHACH HANG XE HOI CHUNG KHOANG\\4000 Chung Khoang HCM.xlsx']
    print(f"num paths : {len(files_path)}")
    people = []
    count = 0
    for num_path, path in enumerate(files_path):
        try:
            print(path)
            if num_path / 100 == 0:
                print(
                    "-----------------------------------------------------------------------------------------------------------------")
                print(f"run path : {num_path}")
                print(
                    "-----------------------------------------------------------------------------------------------------------------")
            file_path = input_folder + path.replace("\\", "/")
            print(file_path)
            sheets_names = list(set(tag_result_df[tag_result_df["old_path"] == path]["sheetname"].values))
            results_file_df = tag_result_df[tag_result_df["old_path"] == path]
            if len(sheets_names) > 0:
                for sheet_name in sheets_names:
                    df = readfile(file_path, sheet_name)
                    df = run_preprocess_cols(df, results_file_df, sheet_name)
                    df.dropna(axis=0, inplace=True, how="all")
                    df.dropna(axis=1, inplace=True, how="all")
                    if "điện thoại" not in df.columns:
                        print(f"Không có điện thoại: {path}")
                        continue
                    for index in df.index:
                        row = df.iloc[index]
                        person = run_process_person(row)
                        if person.phone_number != None:
                            # person_json = es.fetch(person.phone_number)
                            # if person_json is not None:
                            #     person = priority_field(person, person_json)
                            people.append(person.to_json())
                            if len(people) >= 10000:
                                count += len(people)
                                print(count)
                                es.bulk_insert(people)
                                people = []
                                print(0)

            else:
                df = readfile(file_path)
                df = run_preprocess_cols(df, tag_result_df)
                if "điện thoại" not in df.cols:
                    continue
                for index in df.index:
                    row = df.iloc[index]
                    person = run_process_person(row)
                    if person.phone_number != None:
                        # person_json = es.fetch(person.phone_number)
                        # if person_json is not None:
                        #     person = priority_field(person, person_json)
                        people.append(person.to_json())
                        if len(people) >= 10000:
                            es.bulk_insert(people)
                            people = []
                            print(0)
        except Exception as e:
            print(e)
            with open("tmp/sdt_error_1.txt", "w") as f:
                f.write(path)
                f.write("\n")


# def insert_phone_to_es():
#     es = ElasticSearchConnection()
#     tag_result_df = load_result_df()
#     files_path = list(set(tag_result_df["old_path"].values))
#     # files_path = ['\\ALL DATA\\DATA KHACH HANG\\KHACH HANG XE HOI CHUNG KHOANG\\4000 Chung Khoang HCM.xlsx']
#     print(f"num paths : {len(files_path)}")
#     for num_path, path in enumerate(files_path):
#         try:
#             print(path)
#             if num_path / 100 == 0:
#                 print("-----------------------------------------------------------------------------------------------------------------")
#                 print(f"run path : {num_path}")
#                 print(
#                     "-----------------------------------------------------------------------------------------------------------------")
#             file_path = input_folder + path.replace("\\", "/")
#             print(file_path)
#             sheets_names = list(set(tag_result_df[tag_result_df["old_path"] == path]["sheetname"].values))
#             results_file_df = tag_result_df[tag_result_df["old_path"] == path]
#             if len(sheets_names) > 0:
#                 for sheet_name in sheets_names:
#                     df = readfile(file_path, sheet_name)
#                     df = run_preprocess_cols(df, results_file_df, sheet_name)
#                     df = df.dropna(axis=0, how="all",  inplace=True)
#                     df = df.dropna(axis=1, how="all", inplace=True)
#                     if "điện thoại" not in df.columns:
#                         print(f"Không có điện thoại: {path}")
#                         continue
#                     for index in df.index:
#                         row = df.iloc[index]
#                         person = run_process_person(row)
#                         if person.phone_number != None:
#                             es.insert_phone(person.phone_number)
#             else:
#                 df = readfile(file_path)
#                 df = run_preprocess_cols(df, tag_result_df)
#                 if "điện thoại" not in df.cols:
#                     continue
#                 for index in df.index:
#                     row = df.iloc[index]
#                     person = run_process_person(row)
#                     if person is not None:
#                         es.insert_phone(person.phone_number)
#         except Exception as e:
#             print(e)
#             with open("tmp/sdt_error_1.txt", "w") as f:
#                 f.write(path)
#                 f.write("\n")
insert_to_es()

# if __name__ == '__main__':
#     es = ElasticSearchConnection()
#     tag_result_df = load_result_df()
#     files_path = list(set(tag_result_df["old_path"].values))
#     print(f"num paths : {len(files_path)}")
#     for num_path, path in enumerate(files_path):
#         if num_path / 100 == 0:
#             print(f"run path : {num_path}")
#         try:
#             file_path = input_folder + path.replace("\\", "/")
#             print(file_path)
#             sheets_names = list(set(tag_result_df[tag_result_df["old_path"] == path]["sheetname"].values))
#             results_file_df = tag_result_df[tag_result_df["old_path"] == path]
#             if len(sheets_names) > 0:
#                 for sheet_name in sheets_names:
#                     df = readfile(file_path, sheet_name)
#                     df = run_preprocess_cols(df, results_file_df, sheet_name)
#                     df.dropna(axis=0, how="all",  inplace=True)
#                     df.dropna(axis=1, how="all", inplace=True)
#                     if "điện thoại" not in df.cols:
#                         continue
#                     for index in df.index:
#                         row = df.iloc[index]
#                         person = run_process_person(row)
#
#                         es.insert(person)
#             else:
#                 df = readfile(file_path)
#                 df = run_preprocess_cols(df, tag_result_df)
#                 if "điện thoại" not in df.cols:
#                     continue
#                 for index in df.index:
#                     row = df.iloc[index]
#                     person = run_process_person(row)
#                     es.insert(person)
#         except Exception as e:
#             print(e)
#             print(path)
#             with open("tmp/error.txt", "w") as f:
#                 f.write(path)
#                 f.write("\n")
