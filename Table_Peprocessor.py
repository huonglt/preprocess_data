import numpy as np


class TablePreprocessor:
    def __init__(self):
        pass

    def detect_header_columns(self, df):
        remove_index = []
        titles = []
        for i in range(len(df)):
            row = df.loc[i]
            if row[0] is not np.nan:
                try:
                    float(row[0])
                    remove_index.append(i)
                except:
                    tmp_items = []
                    for item in row:
                        if type(item) is str:
                            tmp_items.append(item)
                    if len(tmp_items) <= 2:
                        titles += tmp_items
                        remove_index.append(i)
                    else:
                        break
            else:
                remove_index.append(i)
        df.drop(df.index[remove_index], inplace=True)
        new_header = df.iloc[0]
        df = df[1:]
        df.columns = new_header
        df.reset_index(drop=True, inplace=True)
        return df, titles

    def check_cols_is_nan(self, df):
        for col in df.columns:
            if df[col].notnull().sum() <= 2:
                del df[col]
        return df


